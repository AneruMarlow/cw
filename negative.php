<?php
function makeNegative(float $num) : float {
    return ($num>0 ? -$num : $num);
}

/*
function makeNegative(float $num) : float {
    return -abs($num);
}

In this simple assignment you are given a number and have to make it negative. But maybe the number is already negative?
*/