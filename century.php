<?php
function centuryFromYear($y):int
{
    return ($y % 100 != 0 ? $y / 100 + 1 : $y / 100);
}


//   $c=$y/100;
//   if ($y%100!=0) {
//   $c++;
//   }
//   return $c;
/*
function centuryFromYear($year)
{
  return ceil($year / 100);
}

The first century spans from the year 1 up to and including the year 100,
The second - from the year 101 up to and including the year 200, etc.
*/